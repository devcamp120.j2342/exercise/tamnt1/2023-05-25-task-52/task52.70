import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;

public class App {
    public static void main(String[] args) throws Exception {
        ArrayList<Order2> orderList = new ArrayList<>();
        Person person0 = new Person("Tam", 20, 100000000);
        Person person1 = new Person("Minh", 25, "70 kg", 20000000);

        Order2 order1 = new Order2(1, "Tam", 500000, new Date(), true,
                new String[] { "Item 1", "Item 2" }, person0);

        Order2 order2 = new Order2(2, "Minh", 200000, new Date(), true,
                new String[] { "Item 1", "Item 2" }, person1);
        orderList.add(order1);
        orderList.add(order2);

        for (Order2 order : orderList) {
            System.out.println(order);
        }

    }

    // subTask1
    public static class Order2 {
        private int id;
        private String customerName;
        private Integer price;
        private Date orderDate;
        private Boolean confirm;
        private String[] items;
        private Person buyer;

        // subTask2
        public Order2(int id, String customerName, Integer price, Date orderDate, Boolean confirm, String[] items,
                Person buyer) {
            this.id = id;
            this.customerName = customerName;
            this.price = price;
            this.orderDate = orderDate;
            this.confirm = confirm;
            this.items = items;
            this.buyer = buyer;
        }

        public Order2(int id, String customerName, Integer price, Date orderDate, Boolean confirm, String[] items) {
            this.id = id;
            this.customerName = customerName;
            this.price = price;
            this.orderDate = orderDate;
            this.confirm = confirm;
            this.items = items;
            this.buyer = null;
        }

        public Order2(int id, String customerName, Integer price, Date orderDate, Boolean confirm) {
            this.id = id;
            this.customerName = customerName;
            this.price = price;
            this.orderDate = orderDate;
            this.confirm = confirm;
            this.items = new String[0];
            this.buyer = null;
        }

        public Order2(int id, String customerName, Integer price, Date orderDate) {
            this.id = id;
            this.customerName = customerName;
            this.price = price;
            this.orderDate = orderDate;
            this.confirm = false;
            this.items = new String[0];
            this.buyer = null;
        }

        // subTask4
        @Override
        public String toString() {
            // subTask3
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd'-'MMMM'-'yyyy", new Locale("vi", "VN"));
            String formattedOrderDate = dateFormat.format(orderDate);
            NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(Locale.forLanguageTag("vi-VN"));
            String formattedPrice = currencyFormat.format(price);

            StringBuilder builder = new StringBuilder();
            builder.append("Order2 [id=").append(id)
                    .append(", customerName=").append(customerName)
                    .append(", price=").append(formattedPrice)
                    .append(", orderDate=").append(formattedOrderDate)
                    .append(", confirm=").append(confirm ? "Yes" : "No")
                    .append(", items=").append(Arrays.toString(items));

            if (buyer != null) {
                builder.append(", buyer=").append(buyer.toString());
            }

            builder.append("]");

            return builder.toString();
        }

    }

    public static class Person {
        private String name;
        private int age;
        private String weight;
        private long salary;
        private String[] pets;

        // Subtask1
        public Person(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public Person(String name, int age, String weight) {
            this.name = name;
            this.age = age;
            this.weight = weight;
        }

        public Person(String name, int age, long salary) {
            this.name = name;
            this.age = age;
            this.salary = salary;
        }

        public Person(String name, int age, String weight, long salary) {
            this.name = name;
            this.age = age;
            this.weight = weight;
            this.salary = salary;
        }

        public Person(String name, int age, String weight, long salary, String[] pets) {
            this.name = name;
            this.age = age;
            this.weight = weight;
            this.salary = salary;
            this.pets = pets;
        }

        // Subtask4
        @Override
        public String toString() {
            NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(Locale.forLanguageTag("vi-VN"));
            String formattedSalary = currencyFormat.format(salary);

            return "Person [name=" + name + ", age=" + age + ", weight=" + weight + ", salary=" + formattedSalary
                    + ", pets="
                    + Arrays.toString(pets) + "]";
        }

    }
}